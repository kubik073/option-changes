{if $smarty.request.product_id}
<div class="control-group">
    <label class="control-label" for="elm_option_main_option_{$id}">{__("main_option")}</label>
    <div class="controls">
        <input type="hidden" name="option_data[main_option]" value="N" />
        <input type="checkbox" name="option_data[main_option]" id="elm_option_main_option_{$id}" value="Y" {if $option_data.main_option == "Y"}checked{/if} />
    </div>
</div>
{/if}