{if ($product.price|floatval || $product.zero_price_action == "P" || $product.zero_price_action == "A" || (!$product.price|floatval && $product.zero_price_action == "R")) && !($settings.General.allow_anonymous_shopping == "hide_price_and_add_to_cart" && !$auth.user_id)}
    {assign var="show_price_values" value=true}
{else}
    {assign var="show_price_values" value=false}
{/if}
{if $product.tracking == "ProductTracking::TRACK_WITH_OPTIONS"|enum}
    {assign var="out_of_stock_text" value=__("text_combination_out_of_stock")}
{else}
    {assign var="out_of_stock_text" value=__("text_out_of_stock")}
{/if}

{if $runtime.controller == "auth" && $runtime.mode == "login_form"}
    {assign var="login_url" value=$config.current_url}
{else}
    {assign var="login_url" value="auth.login_form?return_url=`$c_url`"}
{/if}

<div class="ty-control-group ty-product-options__item{if !$capture_options_vs_qty} product-list-field{/if} clearfix" id="opt_{$obj_prefix}{$id}_{$po.option_id}">
    <table class="ty-table">
        <thead>
        <tr>
            <th>{$po.option_name}{if $po.description|trim}{include file="common/tooltip.tpl" tooltip=$po.description}{/if}</th>
            <th>{__("product_code")}</th>
            <th>{__("price")}</th>
            <th>{__("quantity")}</th>
            <th>{__("order")}</th>
        </tr>
        </thead>
        <tbody>
        {foreach from=$po.variants item="vr" name=vars}
            <tr class="qb-variant-row">
                <td>{$vr.variant_name}</td>
                <td>{$vr.table_data.product_code}</td>
                {if $show_price_values}
                    <td>
                        
                        <span class="ty-price{if !$vr.table_data.price|floatval && !$product.zero_price_action} hidden{/if}">{include file="common/price.tpl" value=$vr.table_data.price class="ty-price-num"}</span>
                        {if $show_clean_price && $settings.Appearance.show_prices_taxed_clean == "Y" && $vr.table_data.taxed_price}
                            <input type="hidden" name="appearance[show_clean_price]" value="{$show_clean_price}" />
                            {if $vr.table_data.price != $vr.table_data.taxed_price && $product.included_tax}
                                <span class="ty-list-price ty-nowrap">({include file="common/price.tpl" value=$vr.table_data.taxed_price class="ty-list-price ty-nowrap"} {__("inc_tax")})</span>
                            {elseif $vr.table_data.price != $vr.table_data.taxed_price && !$product.included_tax}
                                <span class="ty-list-price ty-nowrap ty-tax-include">({__("including_tax")})</span>
                            {/if}
                        {/if}
                    </td>
                    {assign var="cart_button_exists" value=false}
                    {assign var="product_amount" value=$vr.table_data.amount}
                    {if ($settings.General.allow_anonymous_shopping == "allow_shopping" || $auth.user_id) && !($product.zero_price_action == "R" && $vr.table_data.price == 0) && !($settings.General.inventory_tracking == "Y" && $settings.General.allow_negative_amount != "Y" && (($product_amount <= 0 || $product_amount < $product.min_qty) && $product.tracking != "ProductTracking::DO_NOT_TRACK"|enum) && $product.is_edp != "Y")}
                        {if !($product.has_options && !$show_product_options && !$details_page)}
                            {assign var="cart_button_exists" value=true}
                        {/if}
                    {/if}
                    {if $cart_button_exists > 0}
                        <td>
                            {if !empty($product.selected_amount)}
                                {assign var="default_amount" value=$product.selected_amount}
                            {elseif !empty($product.min_qty)}
                                {assign var="default_amount" value=$product.min_qty}
                            {elseif !empty($product.qty_step)}
                                {assign var="default_amount" value=$product.qty_step}
                            {else}
                                {assign var="default_amount" value="1"}
                            {/if}
                             <input type="hidden" name="{$name}[{$id}][product_options][{$po.option_id}]" value="{$vr.variant_id}"
                            <div class="ty-qty clearfix{if $settings.Appearance.quantity_changer == "Y"} changer{/if}">
                                {if $product.qty_content}
                                    <select name="product_data[{$obj_id}][amount]" id="qty_count_{$obj_prefix}{$obj_id}_{$vr.variant_id}">
                                    {assign var="a_name" value="product_amount_`$obj_prefix``$obj_id`"}
                                    {assign var="selected_amount" value=false}
                                    {foreach name="`$a_name`" from=$product.qty_content item="var"}
                                        <option value="{$var}" {if $product.selected_amount && ($product.selected_amount == $var || ($smarty.foreach.$a_name.last && !$selected_amount))}{assign var="selected_amount" value=true}selected="selected"{/if}>{$var}</option>
                                    {/foreach}
                                    </select>
                                {else}
                                <div class="ty-center ty-value-changer cm-value-changer">
                                    {if $settings.Appearance.quantity_changer == "Y"}
                                        <a class="cm-increase ty-value-changer__increase">&#43;</a>
                                    {/if}
                                    <input {if $product.qty_step > 1}readonly="readonly"{/if} type="text" size="5" class="ty-value-changer__input cm-amount" id="qty_count_{$obj_prefix}{$obj_id}_{$vr.variant_id}" name="product_data[{$obj_id}][amount]" value="{$default_amount}"{if $product.qty_step > 1} data-ca-step="{$product.qty_step}"{/if} data-ca-min-qty="{if $product.min_qty > 1}{$product.min_qty}{else}1{/if}" />
                                    {if $settings.Appearance.quantity_changer == "Y"}
                                        <a class="cm-decrease ty-value-changer__decrease">&minus;</a>
                                    {/if}
                                </div>
                                {/if}
                            </div>
                        </td>
                        <td>
                            {include file="buttons/add_to_cart.tpl" but_id="button_cart_`$obj_prefix``$obj_id`" but_name="dispatch[checkout.add..`$obj_id`]" but_role=$but_role block_width=$block_width obj_id=$obj_id product=$product but_meta="`$add_to_cart_meta` qb-variant-button"}
                        </td>
                    {else}
                        <td colspan="2">
                            <span class="ty-qty-out-of-stock ty-control-group__item">
                            {if ($settings.General.allow_anonymous_shopping != "allow_shopping" && !$auth.user_id)}
                                {include file="buttons/button.tpl" but_id=$but_id but_text=__("sign_in_to_buy") but_href=$login_url but_role=$but_role|default:"text" but_name=""}
                            {else}
                                {$out_of_stock_text}
                            {/if}
                            </span>
                        </td>
                    {/if}
                {else}
                    <td colspan="3">
                        <span class="ty-price">{__("sign_in_to_view_price")}</span>
                    </td>
                {/if}
            </tr>
        {/foreach}
        </tbody>
    </table>

</div>