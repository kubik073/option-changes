{if ($settings.General.display_options_modifiers == "Y" && ($auth.user_id  || ($settings.General.allow_anonymous_shopping != "hide_price_and_add_to_cart" && !$auth.user_id)))}
{assign var="show_modifiers" value=true}
{/if}

<input type="hidden" name="appearance[details_page]" value="{$details_page}" />
{foreach from=$product.detailed_params key="param" item="value"}
    <input type="hidden" name="additional_info[{$param}]" value="{$value}" />
{/foreach}

{if $product_options}

{if $obj_prefix}
    <input type="hidden" name="appearance[obj_prefix]" value="{$obj_prefix}" />
{/if}

{if $location == "cart" || $product.object_id}
    <input type="hidden" name="{$name}[{$id}][object_id]" value="{$id|default:$obj_id}" />
{/if}

{if $extra_id}
    <input type="hidden" name="extra_id" value="{$extra_id}" />
{/if}

{* Simultaneous options *}
{if $product.options_type == "S" && $location == "cart"}
    {$disabled = true}
{/if}

<div id="option_{$id}_AOC">
    <div class="cm-picker-product-options ty-product-options" id="opt_{$obj_prefix}{$id}">
        {foreach name="product_options" from=$product_options item="po"}
            {if $po.main_option == "Y" && "SRC"|strpos:$po.option_type !== false}
                {include file="addons/option_changes/components/option_main.tpl"}
            {else}
                {include file="addons/option_changes/components/option_simple.tpl"}
            {/if}
        
        {/foreach}
    </div>
</div>
{if $product.show_exception_warning == "Y"}
    <p id="warning_{$obj_prefix}{$id}" class="ty-product-options__no-combinations">{__("nocombination")}</p>
{/if}
{/if}

{if !$no_script}
<script type="text/javascript">
(function(_, $) {
    $.ceEvent('on', 'ce.formpre_{$form_name|default:"product_form_`$obj_prefix``$id`"}', function(frm, elm) {
        if ($('#warning_{$obj_prefix}{$id}').length) {
            $.ceNotification('show', {
                type: 'W', 
                title: _.tr('warning'), 
                message: _.tr('cannot_buy'),
            });

            return false;
        }
            
        return true;
    });
}(Tygh, Tygh.$));
</script>
{/if}
<script type="text/javascript">
(function(_, $) {
    $.ceEvent('on', 'ce.formajaxpost_{$form_name|default:"product_form_`$obj_prefix``$id`"}', function(frm, elm) {
        $('.qb-variant-row input').prop('disabled', false);
    });
    $(document).on('click', '.qb-variant-button', function() {
        var p = $(this).parents('.qb-variant-row');
        $('.qb-variant-row input').prop('disabled', true);
        p.find('input').prop('disabled', false);
    });
}(Tygh, Tygh.$));
</script>
