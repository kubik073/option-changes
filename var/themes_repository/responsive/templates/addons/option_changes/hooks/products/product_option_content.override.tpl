{if $details_page && $product.main_option_exists}
{hook name="products:product_option_content"}
{if $disable_ids}
    {assign var="_disable_ids" value="`$disable_ids``$obj_id`"}
{else}
    {assign var="_disable_ids" value=""}
{/if}
{include file="addons/option_changes/components/product_options.tpl" id=$obj_id product_options=$product.product_options name="product_data" capture_options_vs_qty=$capture_options_vs_qty disable_ids=$_disable_ids}
{/hook}
{/if}