{if $product.main_option_exists}
    {capture name="old_price_`$obj_id`"}{/capture}
    {capture name="price_`$obj_id`"}{/capture}
    {capture name="clean_price_`$obj_id`"}{/capture}
    {capture name="list_discount_`$obj_id`"}{/capture}
    
    {capture name="sku_`$obj_id`"}{/capture}
    {capture name="product_amount_`$obj_id`"}{/capture}
    {capture name="qty_`$obj_id`"}{/capture}
    
    <input type="hidden" name="appearance[show_add_to_cart]" value="1" />
    <input type="hidden" name="appearance[show_list_buttons]" value="{$show_list_buttons}" />
    <input type="hidden" name="appearance[but_role]" value="{$but_role}" />
    <input type="hidden" name="appearance[quick_view]" value="{$quick_view}" />
    <input type="hidden" name="appearance[show_price_values]" value="1" />
    <input type="hidden" name="appearance[show_price]" value="1" />
    <input type="hidden" name="appearance[show_qty]" value="1" />
    <input type="hidden" name="appearance[capture_options_vs_qty]" value="{$capture_options_vs_qty}" />
{/if}