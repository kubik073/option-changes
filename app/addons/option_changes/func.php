<?php

use Tygh\Registry;
use Tygh\Enum\ProductTracking;

if (!defined('BOOTSTRAP')) { die('Access denied'); }

function fn_option_changes_update_product_option_pre($option_data, $option_id, $lang_code) {
    if (isset($option_data['main_option']) && $option_data['main_option'] == 'Y' && isset($option_data['product_id']) && $option_data['product_id'] > 0) {
        db_query('UPDATE ?:product_options SET main_option = ?s WHERE product_id = ?i', 'N', $option_data['product_id']);
    }
}

function fn_option_changes_gather_additional_product_data_post(&$product, $auth, $params) {
    if (AREA == 'C' && $params['get_for_one_product'] == true && $params['get_options']) {
        if (isset($product['product_options']) && !empty($product['product_options'])) {
            foreach ($product['product_options'] as $opt_id => $option) {
                if ($option['main_option'] == 'Y') {
                    foreach ($option['variants'] as $var_id => $variant) {
                        $product['product_options'][$opt_id]['variants'][$var_id]['table_data'] = fn_get_variant_table_data($product, $auth, $params, $opt_id, $var_id);
                    }
                    $product['main_option_exists'] = true;
                    break;
                }
            }
        }
    }
}

function fn_get_variant_table_data($product, $auth, $params, $opt_id, $var_id) {
    unset($product['changed_option']);
    $product['selected_options'][$opt_id] = $var_id;
    $product['product_options'][$opt_id]['value'] = $var_id;
    $product['modifiers_price'] = 0;
    $product['price'] = $product['base_price'];
    
    if (isset($product['promotions'])) {
        unset($product['promotions']);
    } 
    if (isset($product['discount'])) {
        unset($product['discount']);
    } 

    $product = fn_apply_options_rules($product);
    
    $product['selected_options'][$opt_id] = $var_id;
    $product['product_options'][$opt_id]['value'] = $var_id;

    $selected_options = isset($product['selected_options']) ? $product['selected_options'] : array();
    foreach ($product['product_options'] as $option) {
        if (!empty($option['disabled'])) {
            unset($selected_options[$option['option_id']]);
        }
    }
    $product['selected_options'] = $selected_options;

    // Change price
    if (isset($product['price']) && empty($product['modifiers_price'])) {
        $product['base_modifier'] = fn_apply_options_modifiers($selected_options, $product['base_price'], 'P', array(), array('product_data' => $product));
        $old_price = $product['price'];
        $product['price'] = fn_apply_options_modifiers($selected_options, $product['price'], 'P', array(), array('product_data' => $product));

        if (empty($product['original_price'])) {
            $product['original_price'] = $old_price;
        }

        $product['original_price'] = fn_apply_options_modifiers($selected_options, $product['original_price'], 'P', array(), array('product_data' => $product));
        $product['modifiers_price'] = $product['price'] - $old_price;
    }

    if (!empty($product['prices']) && is_array($product['prices'])) {
        foreach ($product['prices'] as $pr_k => $pr_v) {
            $product['prices'][$pr_k]['price'] = fn_apply_options_modifiers($selected_options, $pr_v['price'], 'P', array(), array('product_data' => $product));
        }
    }
    
    fn_set_hook('gather_additional_product_data_before_discounts', $product, $auth, $params);

    // Get product discounts
    if ($params['get_discounts'] && !isset($product['exclude_from_calculate'])) {
        fn_promotion_apply('catalog', $product, $auth);
        if (!empty($product['prices']) && is_array($product['prices'])) {
            $product_copy = $product;
            foreach ($product['prices'] as $pr_k => $pr_v) {
                $product_copy['base_price'] = $product_copy['price'] = $pr_v['price'];
                fn_promotion_apply('catalog', $product_copy, $auth);
                $product['prices'][$pr_k]['price'] = $product_copy['price'];
            }
        }
    }

    $table_data = array(
        'product_code' => $product['product_code'],
        'price' => $product['price']
    );
    if (!empty($product['tracking']) && $product['tracking'] == ProductTracking::TRACK_WITH_OPTIONS) {
        $table_data['amount'] = $product['inventory_amount'];
    } else {
        $table_data['amount'] = $product['amount'];
    }
    
    $allow_negative_amount = Registry::get('settings.General.allow_negative_amount');
    $inventory_tracking = Registry::get('settings.General.inventory_tracking');
    if ($allow_negative_amount == 'Y' || $inventory_tracking == 'N') {
        $table_data['amount'] = max(1, $product['min_qty']);
    }

    // Add product prices with taxes and without taxes
    if ($params['get_taxed_prices'] && AREA != 'A' && Registry::get('settings.Appearance.show_prices_taxed_clean') == 'Y' && $auth['tax_exempt'] != 'Y') {
        fn_get_taxed_and_clean_prices($product, $auth);
        $table_data['taxed_price'] = $product['taxed_price'];
    }
    
    
    if (!empty($product['prices']) && is_array($product['prices'])) {
        $table_data['prices'] = $product['prices'];
    }
    return $table_data;
}