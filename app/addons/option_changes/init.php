<?php

if (!defined('BOOTSTRAP')) { die('Access denied'); }

fn_register_hooks(
    'update_product_option_pre',
    'gather_additional_product_data_post'
);